import 'package:flutter/material.dart';

class TipProvider {
  void showRandomTip(BuildContext context) {
    SnackBar snack = const SnackBar(
      content: Text(
        "You can water your plants by tapping on them",
        style: TextStyle(color: Colors.black),
      ),
      shape: RoundedRectangleBorder(),
      duration: Duration(seconds: 10),
      backgroundColor: Colors.greenAccent,
      showCloseIcon: true,
      closeIconColor: Colors.black,
    );
    var messager = ScaffoldMessenger.maybeOf(context);
    if (messager == null) {
      return;
    }
    messager.showSnackBar(snack);
  }
}
