String changePlantSize(String plant, String size) {
  return plant.substring(0, 15) + size + plant.substring(plant.length - 4);
}

String getPlantSize(String plant) {
  return plant.substring(15, plant.length - 4);
}

String changePlantType(String plant, int type) {
  return plant.substring(0, 12) +
      type.toString().padLeft(3, '0') +
      plant.substring(15);
}

String getPlantType(String plant) {
  return plant.substring(15, plant.length - 4);
}

String growPlant(String plant) {
  if (plant.length <= 18) {
    return plant;
  }
  String curSize = getPlantSize(plant);
  if (curSize == "Sprout") {
    plant = changePlantSize(plant, "Medium");
  } else if (curSize == "Medium") {
    plant = changePlantSize(plant, "Full");
  }
  return plant;
}
