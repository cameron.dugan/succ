import 'storage.dart';
import 'package:flutter/material.dart';

void firstDayDoneDialogue(context, Storage s) async {
  var seenAlready = await s.readSeenTutorial();
  if (seenAlready) {
    return;
  }
  showDialog<String>(
    context: context,
    builder: (BuildContext context) => AlertDialog(
      title: const Text('Congratulations!'),
      content: const Text(
          'The first day is done, come back tomorrow to see your plant grow!'),
      actions: <Widget>[
        TextButton(
          onPressed: () => Navigator.pop(context, 'OK'),
          child: const Text('OK'),
        ),
      ],
    ),
  );
  s.writeSeenTutorial(true);
}
